# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# Parameters, Return, Example, Pseudocode.
# P - number, will always be a number
# R - return a string - "Buzz" if divisible by 5 if not return the parameter
# example - 
#pseduocode - if 5 then return a string "Buzz", if not then return the number you passed into function


def is_divisible_by_5(number):
    if number % 5 == 0:
        return "Buzz"
    else: 
        return number

print(is_divisible_by_5(50), "Buzz")
print(is_divisible_by_5(4), "4")


