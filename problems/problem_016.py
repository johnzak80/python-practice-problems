# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x >= 0 and x <= 10 and y >= 0 and y <= 10:
        return "In Bounds"
    else:
        return "Out of Bounds"
    
print(is_inside_bounds(10,9), True)
print(is_inside_bounds(15,-9), False)