# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    
    if sorted(ingredients) == sorted(["flour", "eggs", "oil"]):
        return True
    else:
        return False
    
print(can_make_pasta(["oil", "eggs", "flour"]), True)
print(can_make_pasta(["poo", "tears", "sweat"]), False)


alpha_ing = ["oil", "eggs", "flour"].sort()
print(alpha_ing)