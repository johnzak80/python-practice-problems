# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    count = 0
    for num in values:
        count += num
    
    average = count / len(values)
    if values == []:
        return None
    else:
        return int(average)
    
print(calculate_average([3,6,3]))
        
