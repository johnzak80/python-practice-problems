# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):

    return sum(values)

    
print(calculate_sum([4,5,6]), 15)
print(calculate_sum([7,8,9]), 24)
